# python-modules-ML

---

# Requirements



## CPU (Intel/AMD):
### Hardware requirements:
* ``Intel/AMD 64-bit architecture``,generally named as ``x86-64``.
* Architecture must support ``AVX2`` instructions.

### Software requirements:
* ### ONEDNN [https://github.com/oneapi-src/oneDNN](https://github.com/oneapi-src/oneDNN)

    If have ``conda`` installed,download latest version using ``conda``.

    ```
    conda install -c conda-forge onednn 
    ```
    
    ##### OR

 
    Windows: [https://github.com/oneapi-src/oneDNN/releases/download/v2.1/dnnl_win_2.1.0_cpu_vcomp.zip](https://github.com/oneapi-src/oneDNN/releases/download/v2.1/dnnl_win_2.1.0_cpu_vcomp.zip)

    * Extract downloaded zip file,navigate to the ``bin`` directory,copy the ``dnnl.dll`` into the ``system's PATH``.

    Linux: [https://github.com/oneapi-src/oneDNN/releases/download/v2.1/dnnl_lnx_2.1.0_cpu_gomp.tgz](https://github.com/oneapi-src/oneDNN/releases/download/v2.1/dnnl_lnx_2.1.0_cpu_gomp.tgz)
        

    * NOTE: make sure corresponding downloaded ``shared libraries named as libdnnl.so.x.x`` using direct-links are in ``system's PATH``.


* ### OPENBLAS [https://github.com/xianyi/OpenBLAS/](https://github.com/xianyi/OpenBLAS/).
    * Download for windows: [https://github.com/xianyi/OpenBLAS/releases/download/v0.3.19/OpenBLAS-0.3.19-x64.zip](https://github.com/xianyi/OpenBLAS/releases/download/v0.3.19/OpenBLAS-0.3.19-x64.zip) 
        * Extract downloaded zip ,navigate to the ``bin`` directory,copy the ``libopenblas.dll`` into the ``system's PATH`` and rename it as ``openblas.dll``
    * On Linux/debian install it using package manager.
        * On ubuntu this should be enough: ``sudo apt-get install libopenblas-dev``

---

## GPU (CUDA/NVIDIA):
### Hardware requirements:
* For now we support only ``Nvidia based graphic cards``.

### Software Requirements:

* #### CUDA_TOOLKIT available at: [https://developer.nvidia.com/cuda-toolkit-archive](https://developer.nvidia.com/cuda-toolkit-archive)
    Supported toolkits:
    
    * CUDA_TOOLKIT_10.x.x

    * CUDA_TOOLKIT_11.x.x

* #### CUDNN:
    
    #### Download corresponding ``CUDNN`` version depending upon your ``CUDA_TOOLKIT`` and ``Operating system`` from:
    ```
    conda install -c conda-forge cudnn
    ```

    OR

    * direct file download from  [https://anaconda.org/conda-forge/cudnn](https://anaconda.org/conda-forge/cudnn)
    
    OR

    * from NVIDIA (login required)  [https://developer.nvidia.com/cudnn](https://developer.nvidia.com/cudnn)

    Supported CUDNN library:
    
    * CUDNN_7.x.x (with corresponding Cuda toolkit.)
    
    * CUDNN_8.x.x (with corresponding Cuda toolkit.) 

---

## Hybrid (Intel/AMD + GPU(CUDA/NVIDIA)):

All the requirements for both of the compute platforms as mentioned above must be fulfilled for ``hybrid`` models.


*If you are experiencing issues during installation of  dependencies please reach out to support@ramanlabs.in .* 
